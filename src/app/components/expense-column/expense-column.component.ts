import { AfterViewChecked, Component, OnInit } from '@angular/core';
import { Memo } from 'src/app/modals/memos';
import { BudgetAppService } from "../../service/budget-app.service";
import { map, tap ,count, filter, toArray} from "rxjs/operators";
import { Subject } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { AddComponent } from '../add/add.component';
import { UpdateComponent } from '../update/update.component';


@Component({
  selector: 'app-expense-column',
  templateUrl: './expense-column.component.html',
  styleUrls: ['./expense-column.component.css']
})
export class ExpenseColumnComponent implements OnInit {

  totalExp:number=0;
  Expenses:any;
  ExpenseArr:any[]=[]
  constructor(private budgetAppService: BudgetAppService,public dialog: MatDialog) { }

  ngOnInit(): void {
    this.budgetAppService.refresh.subscribe(()=>{
      this.getExpenses()
    })
    this.getExpenses();
  }

  getExpenses(){
    this.budgetAppService.getEntry()
    .subscribe((expense: any)=>{
        this.Expenses=expense;
        for (let i = 0; i < expense.length; i++) {
          const element = expense[i];
          if (element.amount < 0) {
            this.totalExp=this.totalExp+element.amount
          }
        }
        // this.totalExp=this.budgetAppService.getTotalAmount(this.Expenses);
        this.budgetAppService.totalExpense.next(this.totalExp);
    })
  }

  update(Entrydata:any){
    const id=Entrydata.id
    this.budgetAppService.getEntryById(id).subscribe((res)=>{
      const dialogRef = this.dialog.open(UpdateComponent,{
        data:Entrydata
      });
    });
  }
}
