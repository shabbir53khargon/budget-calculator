import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Memo } from 'src/app/modals/memos';
import { BudgetAppService } from 'src/app/service/budget-app.service';


@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  budgetForm = new FormGroup({
    amount: new FormControl(null),
    narration: new FormControl(''),
  });
  constructor(private budgetService:BudgetAppService) { }
  ngOnInit(): void {
  }

onSubmit(){
    this.budgetService.postData(this.budgetForm.value).subscribe(()=>{
      console.warn("data added");
    })
  }
}

