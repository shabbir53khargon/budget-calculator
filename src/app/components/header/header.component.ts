import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import { Memo } from 'src/app/modals/memos';
import { BudgetAppService } from 'src/app/service/budget-app.service';
import { AddComponent } from '../add/add.component';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  result:number
  TotalIncome:number
  TotalExpense:number
  constructor(public dialog: MatDialog,private budgetAppService: BudgetAppService) {

   }
  ngOnInit() {
    // this.result=this.TotalIncome-this.TotalExpense;
    // console.log(this.result)
    this.budgetAppService.refresh.subscribe(()=>{
      this.calculateResult();
    })
    this.calculateResult();
  }

  calculateResult(){
    this.budgetAppService.totalIncome.subscribe(res=>{
      this.TotalIncome=res;
      console.log(this.TotalIncome);
    })
    this.budgetAppService.totalExpense.subscribe(res=>{
      this.TotalExpense=res;
      console.log(this.TotalExpense);
    })
  }

  openDialog() {
    const dialogRef = this.dialog.open(AddComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      // this.budgetAppService.postData(result).subscribe(()=>
      //   console.warn("added new entry")
      //   )
    });
  }


}
