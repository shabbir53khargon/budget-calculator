import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BudgetAppService } from 'src/app/service/budget-app.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<UpdateComponent>, @Inject(MAT_DIALOG_DATA) public data: any,private budgetService:BudgetAppService) { }
  updateForm = new FormGroup({
    amount: new FormControl(null),
    narration: new FormControl(''),
  });
  ngOnInit(): void {
    this.updateForm.patchValue(this.data);
  }
  onSubmit(){
    const id=this.data.id
    this.budgetService.putData(id,this.updateForm.value).subscribe(()=>{
      console.warn("data updated");
    })
  }

}
