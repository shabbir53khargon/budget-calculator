import { AfterContentChecked, AfterViewChecked, Component, DoCheck, Input, OnChanges, OnInit } from '@angular/core';
import { Memo } from 'src/app/modals/memos';
import { BudgetAppService } from "../../service/budget-app.service";
import { map, tap, count, reduce, filter } from "rxjs/operators";
import { Subject } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { AddComponent } from '../add/add.component';
import { UpdateComponent } from '../update/update.component';


@Component({
  selector: 'app-income-column',
  templateUrl: './income-column.component.html',
  styleUrls: ['./income-column.component.css']
})
export class IncomeColumnComponent implements OnInit {

  totalInc=0
  Incomes: any[];
  amount: number;

  constructor(private budgetAppService: BudgetAppService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.budgetAppService.refresh.subscribe(() => {
      this.getIncome()
    })
    this.getIncome();
  }

  getIncome() {
    this.budgetAppService.getEntry()
      .subscribe((income) => {
        // this.Incomes=income
        this.filterIncome(income)
        console.log(income)
        for (let i = 0; i < income.length; i++) {
          const element = income[i];
          if (element.amount > 0) {
            this.totalInc=this.totalInc+element.amount
          }
        }
        // this.totalInc = this.budgetAppService.getTotalAmount(this.Incomes);
        this.budgetAppService.totalIncome.next(this.totalInc)
      })
  }

  filterIncome(data: any) {
    this.Incomes = data
    console.log(this.Incomes);
    // this.totalInc = this.budgetAppService.getTotalAmount(this.Incomes);
    this.budgetAppService.totalIncome.next(this.totalInc)
  }

  update(Entrydata: any) {
    const id = Entrydata.id
    this.budgetAppService.getEntryById(id).subscribe((res) => {
      const dialogRef = this.dialog.open(UpdateComponent, {
        data: Entrydata
      });
    });
  }

}


