import { Component, OnInit } from '@angular/core';
import { Memo } from '../modals/memos';
import { BudgetAppService } from "../service/budget-app.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  Incomes: Memo[] = []
  Expenses: Memo[] = []
  constructor(private budgetAppService: BudgetAppService) { }

  ngOnInit(): void {

  }

}
