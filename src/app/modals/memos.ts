export interface Memo{
    id:number;
    amount:number;
    narration:string;
    type:string;
}