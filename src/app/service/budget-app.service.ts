import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Memo } from '../modals/memos';
import { observable, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class BudgetAppService {
  URL="http://localhost:3000/Entries"
  totalAmt:number=0;
  constructor(private http:HttpClient) { }

  totalIncome = new Subject<number>();
  totalExpense = new Subject<number>();
  private refreshComp = new Subject<void>();

  Incomes: any[]=[]
  Expenses:any[]=[]
  data:any

  get refresh(){
    return this.refreshComp;
  }
  getEntry(){
    return this.http.get<Memo[]>(this.URL)
  }

  getEntryById(data:any){
    const id=data
    return this.http.get<Memo>(this.URL+'/'+id)
  }

  getTotalAmount(data:any[]){
    // console.log(data)
    let total=0;
    for(let i=0;i<data.length;i++){
      total=total+parseInt(data[i].amount)
    }
    this.totalAmt=total;
    // console.log(this.totalAmt)
    return this.totalAmt;
 }

 postData(data:any){
      return this.http.post(this.URL,data).pipe(
        tap(()=>{
          return this.refreshComp.next()
        })
      )
    }
 

 putData(id: any,data: Memo){
  //  console.log(data)
  //  console.log(id)
      return this.http.put(this.URL+'/'+id,data).pipe(
        tap(()=>{
          return this.refreshComp.next()
        })
    )
   }
 }
